# Installation 
Install client dependencies:
```bash
cd client && npm install
```

Install server dependencies:
```bash
cd server && npm install
```

# Running
## Manual
Build client into ./client/dist folder:
```bash
cd client && npm run build
```
Run server (default serve from ./client/dist folder and mongo connection to localhost:27017):
```bash
cd server && npm run start:prod
```
Go to http://localhost:3000

## Dev
Run server (default on port 3000 and mongo connection to localhost:27017):
```bash
cd server && npm run start
```
Run webpack devServer (with proxy to port 3000):
```bash
cd client && npm run serve
```
Go to http://localhost:8080

# Configuration
## Environment variables
- `SERVER_MONGO_URI` -  connection string for mongodb (default: mongodb://localhost:27017/fundraiseup-test)
- `SERVER_PORT` – server port (default: 3000)
- `SERVER_PUBLIC` – path to client build folder (default: ./client/dist)
- `CLIENT_BASE_API_URL` – base api address (default: / – same origin)
