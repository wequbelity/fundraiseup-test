const validate = schema => async(ctx, next) => {
  const { body } = ctx.request;
  const { error } = schema.validate(body, { presence: 'required' });

  if (error) {
    ctx.status = 422;
    ctx.body = error.details;
  } else {
    await next();
  }
};

module.exports = {
  validate
};
