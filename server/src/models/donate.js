const mongoose = require('mongoose');

const { ALLOWED_CURRENCIES } = require('../constants');

const DonateSchema = new mongoose.Schema({
  currency: {
    type: String,
    enum: ALLOWED_CURRENCIES,
    required: true
  },
  amount: {
    type: Number,
    required: true
  }
}, {
  timestamps: {
    createdAt: 'createdAt',
    updatedAt: false
  },
  versionKey: false
});

const Donate = mongoose.model('donations', DonateSchema);

module.exports = { Donate };
