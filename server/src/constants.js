const ALLOWED_CURRENCIES = ['USD', 'EUR', 'GBP', 'RUB'];

module.exports = {
  ALLOWED_CURRENCIES
};
