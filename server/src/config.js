const path = require('path');

module.exports = {
  mongoUri: process.env.SERVER_MONGO_URI || 'mongodb://localhost:27017/fundraiseup-test',
  port: process.env.SERVER_PORT || 3000,
  publicPath: process.env.SERVER_PUBLIC ? path.resolve(process.env.SERVER_PUBLIC) : path.join(__dirname, '../../client/dist')
};
