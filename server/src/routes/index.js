const { router: donateRouter } = require('./donate');

const initRoutes = app => {
  app.use(donateRouter.routes());
};

module.exports = {
  initRoutes
};
