const Router = require('koa-router');
const Joi = require('joi');

const constatns = require('../constants');
const { validate } = require('../middlewares');
const { Donate } = require('../models');

const router = new Router();

const donateCreateSchema = Joi.object({
  amount: Joi.number().min(1).required(),
  currency: Joi.string().valid(...constatns.ALLOWED_CURRENCIES).required()
});

const createDonate = async ctx => {
  const { body } = ctx.request;

  await Donate.create(body);

  ctx.status = 200;
  ctx.body = {
    ok: true
  };
};

router.post('/donate', validate(donateCreateSchema), createDonate);

module.exports = {
  router
};
