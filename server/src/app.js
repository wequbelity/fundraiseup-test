const Koa = require('koa');
const serve = require('koa-static');
const bodyParser = require('koa-bodyparser');
const mongoose = require('mongoose');

const { initRoutes } = require('./routes');
const config = require('./config');

mongoose.connect(config.mongoUri, {
  useNewUrlParser: true,
  useCreateIndex: true,
  useUnifiedTopology: true
});

const app = new Koa();

app.on('error', error => {
  console.error('Server error', error);
});

app.use(bodyParser());
app.use(serve(config.publicPath));
initRoutes(app);

app.listen(config.port, () => {
  console.log(`Server started at ${config.port} with public path ${config.publicPath}`);
});
