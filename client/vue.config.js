const path = require('path')
const vueSrc = './src'

module.exports = {
  runtimeCompiler: true,
  configureWebpack: {
    resolve: {
      alias: {
        '@': path.resolve(__dirname, vueSrc)
      },
      extensions: ['.js', '.vue', '.json']
    }
  },
  devServer: {
    proxy: 'http://localhost:3000'
  }
}
