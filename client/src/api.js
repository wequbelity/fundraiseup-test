import axios from 'axios'

import * as config from '@/config'

const api = axios.create({
  baseURL: config.BASE_API_URL,
  timeout: 6000
})

const createDonate = ({ amount, currency }) => api.post('/donate', { amount, currency }).then(res => res.data)

export default {
  createDonate
}
