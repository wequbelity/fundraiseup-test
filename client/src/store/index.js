import Vue from 'vue'
import Vuex from 'vuex'

import api from '@/api'

Vue.use(Vuex)

const presets = [40, 100, 200, 1000, 2500, 5000]
const suggestion = 40
const currencies = [
  { name: 'US Dollar', code: 'USD', symbol: '$', rate: 1 },
  { name: 'Euro', code: 'EUR', symbol: '€', rate: 0.897597 },
  { name: 'British Pound', code: 'GBP', symbol: '£', rate: 0.81755 },
  { name: 'Russian Ruble', code: 'RUB', symbol: '₽', rate: 63.461993 }
]
const simpleBeutyNumbers = [1, 2, 5, 10, 12, 15, 20, 30, 40].sort((a, b) => a - b)
const findNearest = (number, array) => {
  let i = 0

  for (;i < array.length; ++i) {
    if (array[i] > number) break
  }

  return array[i - 1]
}
const beutifyNumber = (floatNumber) => {
  const number = Math.floor(floatNumber)
  const maxSimpleNumber = simpleBeutyNumbers[simpleBeutyNumbers.length - 1]
  const stringNumber = String(number)

  if (number <= maxSimpleNumber) {
    return findNearest(number, simpleBeutyNumbers)
  }

  if (number < 500) {
    return Number([stringNumber.slice(0, -1), 0].join(''))
  }

  return Number([stringNumber[0], stringNumber[1] < 5 ? 0 : 5, ...'0'.repeat(stringNumber.length - 2)].join(''))
}

export default new Vuex.Store({
  state: {
    suggestion,
    currencies,
    currency: 'USD'
  },
  getters: {
    presets (state) {
      const currencyConfig = currencies.find(c => c.code === state.currency)

      return presets.map(preset => ({
        amount: beutifyNumber(preset * currencyConfig.rate),
        symbol: currencyConfig.symbol,
        code: currencyConfig.code
      }))
    },
    currencySymbol (state) {
      const currencyConfig = currencies.find(c => c.code === state.currency)

      return currencyConfig.symbol
    }
  },
  mutations: {
    changeCurrency (state, value) {
      const previousCurrency = state.currency

      state.currency = value

      if (state.suggestion) {
        const currencyConfig = currencies.find(c => c.code === state.currency)
        const previousCurrencyConfig = currencies.find(c => c.code === previousCurrency)

        state.suggestion = Math.floor(state.suggestion * currencyConfig.rate / previousCurrencyConfig.rate)
      }
    },
    changeSuggestion (state, value) {
      state.suggestion = value
    }
  },
  actions: {
    submit ({ commit, state }) {
      return api.createDonate({
        amount: state.suggestion,
        currency: state.currency
      })
    }
  }
})
